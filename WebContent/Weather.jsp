<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div style="height: 250px !important; margin-top:10px">
	<div id="map"></div>
	<div id="weather-body"
		style="font-size: 48px; position: absolute; margin-top: -200px; text-align: center;">
	</div>
</div>
<script type="text/javascript">
	$.ajax({
				url : "http://api.openweathermap.org/data/2.5/weather?zip=52556,us&APPID=290a032aacd6f1d444c28411ccbd9131&units=imperial",
				dataType : "json",
				success : function(url) {
					console.log(url);
					var location = url.name;
					var temp_f = url.main.temp;
					var temp_min = url.main.temp_min;
					var temp_max = url.main.temp_max;
					$("#weather-header").html("" + location);
					$("#weather-body").html(temp_f + "�F");
					$("#weather-body-details").html(
							"Minimun temprature:" + temp_min + "�F" + "<br>"
							+ "Maximum temprature:" + temp_max + "�F");
				}
			});

	var map;
	var lat1 = 0;
	var lng1 = 0;
	function initMap() {
		var a = {
			center : {
				lat : lat1,
				lng : lng1
			},
			zoom : 8
		};
		map = new google.maps.Map(document.getElementById('map'), a);
	}

	function GetLocation() {
		var geocoder = new google.maps.Geocoder();
		var address = "52556";
		geocoder.geocode({
			'address' : address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				lat1 = results[0].geometry.location.lat();
				lng1 = results[0].geometry.location.lng();

				initMap();
			}
		});
	};
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-rriJ224uaaFrXXgEY-ioCeClJBrd3iA&callback=initMap"	async defer></script>