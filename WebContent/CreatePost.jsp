<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <div class="row">
				<div class="create-post">
					<div style="padding: 0px;">
						<span class="glyphicon glyphicon-pencil"></span> <span
							style="font-weight: lighter color:#888888">Create a post</span>
						<hr>
					</div>
					<div class="row" style="margin-left: 5px;">
						<div class="col-lg-1" style="margin: 3px;">From:</div>
						<div class="col-lg-4">
							<input type="text" class="form-control" name="from"
								placeholder="From" id="from" >
						</div>
						<div class="col-lg-1" style="margin: 3px;">To:</div>
						<div class="col-lg-4">
							<input type="text" class="form-control" name="where"
								placeholder="Enter destination" id="where">
						</div>
					</div>
					<div class="row, input-post" contenteditable id="postBody"></div>
					<div class="row" style="margin-left: 10px">
						<div class="form-check form-check-inline">
							<label class="form-check-label"> <input
								class="form-check-input" type="radio" name="postType"
								value="offer"> Offer
							</label> <label class="form-check-label"> <input
								class="form-check-input" type="radio" name="postType"
								value="request"> Request
							</label>
						</div>
					</div>
					<div class="row">
						<button type="button" style="float: right; margin-right: 20px;" 
							class="btn btn-primary" id="addPost">Post</button>
					</div>
				</div>

			</div>