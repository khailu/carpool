<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
<title>Carpool</title>
<link rel="stylesheet" media="all" href="css/style.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" media="all" href="css/datetimepicker.css" />
<script type="text/javascript" src="js/datetimepicker.js"></script>
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Carpool</a>
			</div>
			<ul class="nav navbar-nav pull-right">

				<li>
					<form class="navbar-form navbar-left" action="Login" method="post">
						<div class="input-group">

							<span class="ml-1"> <input type="text" id="userName"
								name="userName" class="form-control" placeholder="User name" required="required">
							</span> <span class="ml-1"> <input type="password" id="password"
								name="password" class="form-control" placeholder="Password" required="required">
							</span> <span class="ml-1"> &nbsp;
								<button class="btn btn-primary" type="submit">Login</button>
							</span>
							
							<span class="clearfix error-text-color"  id="loginError">${errorString}</span>
						</div>
					</form>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
				<div>home extra images</div>
			</div>
			<div class="col-sm-4 col-md-4 ">
				<form method="post" action="Register">
					<div class="thumbnail">
						
						<div class="form-group">
							<label for="email">E-mail</label> <input type="email"
								id="email" name="regEmail" class="form-control"
								placeholder="E-mail"  >
								<span class="clearfix error-text-color"  id="invalideEmail">${errorEmail }</span>
						</div>
						
						
						<div class="form-group">
							<label for="regUserName">User Name</label> <input type="text"
								id="regUsername" name="regUsername" class="form-control"
								placeholder="User Name" >
								<span class="clearfix error-text-color"  id="invalidUserName">${errorUserName }</span>
						</div>
						
						<div class="form-group">
							<label for="regPassword">Password</label> <input type="text"
								id="regPassword" name="regPassword" class="form-control"
								placeholder="Password"   >
						</div>
						
						<div class="form-group">
							<label for="regPassword">Confirm Password</label> <input type="text"
								id="regConfPassword" name="regConfPassword" class="form-control"
								placeholder="Confirm Password" >							
								<span class="clearfix error-text-color hide" for="errormsg" id="loginError">Password and confirm password does't match</span>	
						</div>
					
						<div class="form-group">
							<label for="regFullName">Full Name</label> <input type="text"
								id="regFullName" name="regFullName" class="form-control"
								placeholder="Full name" required="required">
						</div>
						<div class="form-group">
							<label for="regGender" id="regGender" name="regGender">Gender</label> <select
								class="form-control" >
								<option value="">Select</option>
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div>
						<div class="form-group">
							<label for="regState">State</label> <!-- <input type="text"
								id="regState" name="regState" class="form-control"
								placeholder="State" required="required"> -->
								
								<select class="form-control" >
								<option value="">Select</option>
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="forCity">City</label> <input type="text"
								id="City" name="regCity" class="form-control"
								placeholder="City" >
						</div>

						<div class="form-group">
							<label for="forStreet">Street</label> <input type="text"
								id="street" name="regStreet" class="form-control"
								placeholder="Street" >
						</div>

						<div class="form-group">
							<label for=zipcode">Zipcode</label> <input type="text"
								id="zipcode" name="regZipcode" class="form-control"
								placeholder="Zipcode" >
						</div>

						<div class="form-group">
							<label for="birthYear">Birth Year</label> <input type="text"
								id="birthYear" name="regBirthYear" class="form-control date-time-picker"
								placeholder="Birth Year" required="required">
						</div>

						<div class="form-group"></div>
						<div class="form-group">
							<button class="btn btn-lg btn-primary btn-block" name="operation"
								value="login" type="submit">Register</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<footer>
		<div class="container text-center">
			<span>� 2017, Carpool team </span>
		</div>
	</footer>
</body>
</html>