<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
<title>Carpool</title>
<link rel="stylesheet" media="all" href="css/style.css" />
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Carpool</a>
			</div>
			<ul class="nav navbar-nav pull-right">
				<li>
					<form class="navbar-form navbar-left" role="search">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search for...">
							<!--<span class="input-group-btn">
									<button class="btn btn-secondary" type="button">Go!</button>
								</span> -->
						</div>
					</form>
				</li>
				<li><a href="javascript:void(0);">&nbsp</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-off"></span></a></li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<div>Profile</div>
			</div>
			<div class="col-sm-6 col-md-6 ">
				<div class="comment">
					<div class="media-left">
						<a href="#"> <img class="media-object" width=100px
							height=100px
							src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PGRlZnMvPjxyZWN0IHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjEyLjUiIHk9IjMyIiBzdHlsZT0iZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQ7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9nPjwvc3ZnPg=="
							alt="Image">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Binderiya</h4>
						<div class="input-box" contenteditable="true">
							lorem ipsum dolor lorem ipsum dolorlorem ipsum dolor
						</div>
						<ul class="breadcrumb">
							<li><a href="#"><i class="glyphicon glyphicon-thumbs-up"></i>
									Like</a></li>
							<li><a href="#"><i class="glyphicon glyphicon-comment"></i>
									Comment</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-3">
				<div>Whether</div>
			</div>
		</div>
	</div>

	<footer>
		<div class="container text-center">
			<span>� 2017, Bibash Kafle <!-- , Kathmandu, Nepal -->
			</span>
		</div>
	</footer>
</body>
</html>