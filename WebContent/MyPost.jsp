<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
<title>Carpool</title>
<link rel="stylesheet" media="all" href="css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="js/MyPost2.js" type="text/javascript"></script>
<script src="js/postComment.js" type="text/javascript"></script>
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

</head>
<body onload="GetLocation()">

<!-- 	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Carpool</a>
			</div>
			<ul class="nav navbar-nav pull-right">
				<li>
					<form class="navbar-form navbar-left" role="search">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search for...">
							<span class="input-group-btn">
									<button class="btn btn-secondary" type="button">Go!</button>
								</span>
						</div>
					</form>
				</li>
				<li><a href="javascript:void(0);">&nbsp</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-off"></span></a></li>
			</ul>
		</div>
	</nav> -->
	
	<jsp:include page="Header.jsp"></jsp:include>

	<div class="container ">
		<div class="col-xs-3">
				<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img
						src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PGRlZnMvPjxyZWN0IHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjEyLjUiIHk9IjMyIiBzdHlsZT0iZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQ7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9nPjwvc3ZnPg=="
						width="200px" height="200px" alt="Image" class="img-responsive"
						alt="">
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">${user.userName}</div>
					<div class="profile-usertitle-job">
						<span class="glyphicon glyphicon-map-marker"></span>${user.city}
					</div>
				</div>
				<div>
					&nbsp;
					<!-- <a href="EditProfile.jsp" class="btn btn-lg btn-primary btn-block mypost-button" role="button" class="show-hand">Edit Profile</a> -->
					<!-- <button type="button"
						class="btn btn-lg btn-primary btn-block mypost-button">My
						Post</button> -->
				</div>

			</div>
		</div>
		<div class="col-sm-6 col-md-6 ">
			<jsp:include page="CreatePost.jsp"></jsp:include>

			<div class="row">
				<div class="col-sm-6 col-md-6 request-offer-control">
					<button class="btn btn-lg btn-primary btn-block active"
						name="operation" value="login" type="button" id="offerMyPost">Offer</button>
				</div>
				<div class="col-sm-6 col-md-6 request-offer-control">
					<button class="btn btn-lg btn-primary btn-block" name="operation"
						value="login" type="button" id="requestMyPost" >Request</button>
				</div>
			</div>

			<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
				<div id="postContainer" class="col-sm-12 col-md-12">
					<div class="media">
						<div class="media-left">
							<a href="#"> <img class="media-object" width=100px
								height=100px
								src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PGRlZnMvPjxyZWN0IHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjEyLjUiIHk9IjMyIiBzdHlsZT0iZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQ7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9nPjwvc3ZnPg=="
								alt="Image">
							</a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Binderiya</h4>
							<span class="clearfix created-date">2017-03-17</span>
							<p>Text messaging, or texting, is the act of composing and
								sending electronic messages, typically consisting of alphabetic
								and numeric characters, between two or more users of mobile
								phones, fixed devices (e.g., desktop computers) or portable
								devices (e.g., tablet computers or smartphones). While text
								messages are usually sent over a phone network, due to the
								convergence between the telecommunication and broadcasting
								industries in the 2000s, text messages</p>
							<ul class="breadcrumb">
								<li><a href="javascript:void(0);" postid="1"
									post-belong-to="Binderiya"><i
										class="glyphicon glyphicon-thumbs-up"></i> </a></li>
								<li><a href="javascript:void(0);" postid="1"
									post-belong-to="Binderiya" class="comment-here"><i
										class="glyphicon glyphicon-comment"></i> </a></li>
								<li><a href="javascript:void(0);" postid="1"
									post-belong-to="Binderiya" class="read-comment"><i
										class="glyphicon glyphicon-list-alt"></i> </a></li>
										
								<li><a href="javascript:void(0);" postid="1"
									post-belong-to="Binderiya" class="read-comment"><i
										class="glyphicon glyphicon-trash"></i> </a></li>
										
							</ul>
						</div>
						
						<div class="comment-list">
								<div class="comment">
									<h4 class="media-heading">Binderiya</h4>
									<span class="clearfix created-date">2017-03-17</span>
									Text messaging, or texting, is the act of composing and
									sending electronic messages, typically consisting of alphabetic
									and numeric characters, between two or more users of mobile
								</div>
								
								<div class="comment">
									<h4 class="media-heading">Binderiya</h4>
									<span class="clearfix created-date">2017-03-17</span>
									Text messaging, or texting, is the act of composing and
									sending electronic messages, typically consisting of alphabetic
									and numeric characters, between two or more users of mobile
								</div>
								
								<div class="comment">
									<h4 class="media-heading">Binderiya</h4>
									<span class="clearfix created-date">2017-03-17</span>
									Text messaging, or texting, is the act of composing and
									sending electronic messages, typically consisting of alphabetic
									and numeric characters, between two or more users of mobile
								</div>
								
							</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-12"></div>
			</div>
		</div>
		<div class="col-sm-3 col-md-3" >
			<jsp:include page="Weather.jsp"></jsp:include>
		</div>
	</div>
	
	<jsp:include page="Footer.jsp"></jsp:include>
</body>
</html>