<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<nav class="navbar navbar-inverse navbar-fixed-top"> <!-- navbar-default navbar-inverse navbar-fixed-top -->
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="Home.jsp" title="Home Page"><span class="glyphicon glyphicon-home"> Carpool</span></a>
			</div>
		
			<ul class="nav navbar-nav pull-right">
				<li><a href="javascript:void(0);" id="createNewPost" role="button" title="Create new post"><span class="glyphicon glyphicon-pencil"></span></a></li>
				
				<li>
					<form class="navbar-form navbar-left" role="search">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search for...">
							<!--<span class="input-group-btn">
									<button class="btn btn-secondary" type="button">Go!</button>
								</span> -->
						</div>
					</form>
				</li>
				<li><a href="javascript:void(0);">&nbsp</a></li>
				<li><a href="#" title="Logout" class="logout"><span class="glyphicon glyphicon-off"></span></a></li>
			</ul>
		</div>
	</nav>
	<div class="header-seperator">&nbsp;</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#createNewPost").click(function(){
				 BootstrapDialog.show({
					    title: 'Create new post',
			            message: $('<div></div>').load('CreatePost.jsp')
			        });
			});
		});
	</script>