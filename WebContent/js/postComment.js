
		$(document).ready(function() {
			$(document).on("click",".comment-here", function(){
				
				var postId = $(this).attr("postid");
				var postBelongTo = $(this).attr("post-belong-to");
				
				BootstrapDialog.show({
		            title: 'Reply to '+postBelongTo,
		            message: $('<textarea class="form-control" id="my-comment-box" placeholder="comment here..."></textarea>'),
		            buttons: [{
		                label: 'Comment',
		                cssClass: 'btn-primary',		                
		                hotkey: 13,
		                action: function(e) {
		                	$.post("AddComment",{"postId":postId,"commentBody":$("#my-comment-box").val()}).done(successAddComment).fail(failAddComment);
		                   console.log($("#my-comment-box").val());
		                   e.close();
		                }
		            }]
		        });
			});
		});

		function successAddComment(){
			
		}
		
		function failAddComment(){
			
		}