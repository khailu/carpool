<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" media="all" href="css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body  onload="GetLocation()">
	<div class="container">
		<div class="row">
			<div id="map-weatherPage"></div>
		</div>

		<div class="row weather-wrapper">

			<div class="col-sm weather">
				<div class="day">Day 1</div>
				<div id="weather-day1" class="temprature"></div>
				<div id="date1" class="date"></div>

			</div>
			<div class="col-sm weather">
				<div class="day">Day 2</div>
				<div id="weather-day2" class="temprature"></div>
				<div id="date2" class="date"></div>

			</div>
			<div class="col-sm weather">
				<div class="day">Day 3</div>
				<div id="weather-day3" class="temprature"></div>
				<div id="date3" class="date"></div>

			</div>
			<div class="col-sm weather">
				<div class="day">Day 4</div>
				<div id="weather-day4" class="temprature"></div>
				<div id="date4" class="date"></div>

			</div>
			<div class="col-sm weather">
				<div class="day">Day 5</div>
				<div id="weather-day5" class="temprature"></div>
				<div id="date5" class="date"></div>

			</div>

		</div>


	</div>
	<script type="text/javascript">
		$
				.ajax({
					url : "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=4d8eab54cb4f46e0b2b122013172103&q=52556&format=json&num_of_days=5&units=imperial",
					dataType : "json",
					type : "POST",
					success : function(data) {
						var temp = data;
						var t = data.data.weather;
						console.log(t);
						
						var location = t

						var date1 = t[0].date;
						var date2 = t[1].date;
						var date3 = t[2].date;
						var date4 = t[3].date;
						var date5 = t[4].date;

						var temp1 = t[0].maxtempF;
						var temp2 = t[1].maxtempF;
						var temp3 = t[2].maxtempF;
						var temp4 = t[3].maxtempF;
						var temp5 = t[4].maxtempF;

						$("#weather-day1").html(temp1 + "�F");
						$("#weather-day2").html(temp2 + "�F");
						$("#weather-day3").html(temp3 + "�F");
						$("#weather-day4").html(temp4 + "�F");
						$("#weather-day5").html(temp5 + "�F");

						$("#date1").html(date1);
						$("#date2").html(date2);
						$("#date3").html(date3);
						$("#date4").html(date4);
						$("#date5").html(date5);
					}
				});
	</script>
	<script>
		var map;
		var lat1 = 0;
		var lng1 = 0;
		function initMap() {

			var a = {
				center : {
					lat : lat1,
					lng : lng1
				},
				zoom : 10
			};
			map = new google.maps.Map(document
					.getElementById('map-weatherPage'), a);
			var marker = new google.maps.Marker({
				position : a.center,
				map : map
			});
		}

		function GetLocation() {
			var geocoder = new google.maps.Geocoder();
			var address = "52556";
			geocoder.geocode({
				'address' : address
			}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					lat1 = results[0].geometry.location.lat();
					lng1 = results[0].geometry.location.lng();

					initMap();
				}
			});
		};
	</script>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-rriJ224uaaFrXXgEY-ioCeClJBrd3iA&callback=initMap">
		
	</script>
</body>
</html>