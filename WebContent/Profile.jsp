<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" http-equiv="Content-Type" content="text/html">
<title>Carpool</title>
<link rel="stylesheet" media="all" href="css/style.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<script src="js/postComment.js" type="text/javascript"></script>
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

</head>
<body onload="GetLocation()">

	<jsp:include page="Header.jsp"></jsp:include>
	<div class="container ">
		<div class="col-xs-3">
				<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img
						src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PGRlZnMvPjxyZWN0IHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjEyLjUiIHk9IjMyIiBzdHlsZT0iZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQ7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9nPjwvc3ZnPg=="
						width="200px" height="200px" alt="Image" class="img-responsive"
						alt="">
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">${user.userName }</div>
					<div class="profile-usertitle-job">
						<span class="glyphicon glyphicon-map-marker"></span>${user.city }
					</div>
				</div>
				<div>
					<a href="Profile.jsp" class="btn btn-lg btn-primary btn-block mypost-button" role="button" class="show-hand">Cancle Edit</a>
					<!-- <button type="button"
						class="btn btn-lg btn-primary btn-block mypost-button">My
						Post</button> -->
				</div>

			</div>
		</div>
		<div class="col-sm-6 col-md-6 ">
			<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
				<div id="postContainer" class="col-sm-12 col-md-12">
					<form method="post" action="Register">
					<div class="thumbnail">
						
						<div class="form-group">
							<label for="email">E-mail</label> <input type="email"
								id="email" name="regEmail" class="form-control"
								placeholder="E-mail" required="required" value="${user.email }" >
								<span class="clearfix error-text-color"  id="invalideEmail">${errorEmail }</span>
						</div>
						
						<div class="form-group">
							<label for="regFullName">Full Name</label> <input type="text"
								id="regFullName" name="regFullName" class="form-control"
								placeholder="Full name" required="required">
						</div>
						
						<div class="form-group">
							<label for="birthYear">Birth Year</label> <input type="text"
								id="birthYear" name="regBirthYear" class="form-control date-time-picker"
								placeholder="Birth Year" required="required">
						</div>
						
						<div class="form-group">
							<label for="regGender" id="regGender" name="regGender">Gender</label> <select
								class="form-control" required="required">
								<option value="">Select</option>
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="regState">State</label> <!-- <input type="text"
								id="regState" name="regState" class="form-control"
								placeholder="State" required="required"> -->
								
								<select class="form-control" required="required">
								<option value="">Select</option>
								<option value="Iowa">Iowa</option>
								<option value="Chicago">Chicago</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="forCity">City</label> <input type="text"
								id="City" name="regCity" class="form-control"
								placeholder="City" required="required">
						</div>

						<div class="form-group">
							<label for="forStreet">Street</label> <input type="text"
								id="street" name="regStreet" class="form-control"
								placeholder="Street" required="required">
						</div>

						<div class="form-group">
							<label for=zipcode">Zipcode</label> <input type="text"
								id="zipcode" name="regZipcode" class="form-control"
								placeholder="Zipcode" required="required">
						</div>

						<div class="form-group"></div>
						<div class="form-group">
							<button class="btn btn-lg btn-primary btn-block" name="operation"
								value="login" type="submit">Update</button>
						</div>
					</div>
				</form>
			
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-12"></div>
			</div>
		</div>
		<div class="col-sm-3 col-md-3" >
			<jsp:include page="Weather.jsp"></jsp:include>
		</div>
	</div>
	
	<jsp:include page="Footer.jsp"></jsp:include>
	
</body>
</html>