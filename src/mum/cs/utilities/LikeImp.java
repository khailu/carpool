package mum.cs.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import mum.cs.model.Like;

public class LikeImp {
	

	public static Like saveLike(Like like ,Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			PreparedStatement preparedStmt = con.prepareStatement(
					"insert into likes (likeid, username, postid, datecreated, dateupdated) values(?,?,?,?,?)");
			preparedStmt.setInt(1, like.getLikeId());
			preparedStmt.setString(2, like. getUserName());
			preparedStmt.setInt(3, like.getPostId());
			preparedStmt.setDate(4, like.getDatecreated());
			preparedStmt.setDate(5, like.getDateupdated());
			preparedStmt.execute();

			// execute the preparedstatement
			// preparedStmt.execute();
			//con.close();
			return like;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static boolean deleteLike(Like like, Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			st = con.createStatement();
			PreparedStatement stmt = con.prepareStatement("delete from likes where likeid = ?");
			stmt.setInt(1, like.getLikeId());
			stmt.execute();
			//con.close();
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}




	public static ArrayList<Like> getLikeByUserName(String userName, Connection con) {
		
		ArrayList<Like> likes = new ArrayList<Like>();

	
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select likeid, username, postid, datecreated, dateupdated from likes where username = ?");
			preparedStmt.setString(1, userName);
			rs = preparedStmt.executeQuery();

			// likeloyee like;
			ArrayList<String> record;
			while (rs.next()) {
				Like like = new Like();

				// record = new ArrayList<String>();

				like.setLikeId(rs.getInt(1));
				like.setUserName(rs.getString(2));
				like.setPostId(rs.getInt(3));
				like.setDatecreated(rs.getDate(4));
				like.setDateupdated(rs.getDate(5));
		
				likes.add(like);
				// likes.add(record);
			}
		//	con.close();
			return likes;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}



	public static ArrayList<Like> getLikeByPostId(String postId, Connection con) {
		
		ArrayList<Like> likes = new ArrayList<Like>();

	
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select likeid, username, postid, datecreated, dateupdated from likes where postid = ?");
			preparedStmt.setString(1, postId);
			rs = preparedStmt.executeQuery();

			// likeloyee like;
			ArrayList<String> record;
			while (rs.next()) {
				Like like = new Like();

				// record = new ArrayList<String>();

				like.setLikeId(rs.getInt(1));
				like.setUserName(rs.getString(2));
				like.setPostId(rs.getInt(3));
				like.setDatecreated(rs.getDate(4));
				like.setDateupdated(rs.getDate(5));
		
				likes.add(like);
				// likes.add(record);
			}
		//	con.close();
			return likes;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	
	public static ArrayList<Like> getAllLike(Connection con) {

		ArrayList<Like> likes = new ArrayList<Like>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select likeid, username, postid, datecreated, dateupdated from likes");
			// preparedStmt.setString(1, likeId);
			rs = preparedStmt.executeQuery();

			// likeloyee like;
			ArrayList<String> record;
			while (rs.next()) {
				Like like = new Like();


				like.setLikeId(rs.getInt(1));
				like.setUserName(rs.getString(2));
				like.setPostId(rs.getInt(3));
				like.setDatecreated(rs.getDate(4));
				like.setDateupdated(rs.getDate(5));
		
				likes.add(like);
			}
		//	con.close();
			return likes;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


}
