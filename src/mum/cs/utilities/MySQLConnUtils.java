package mum.cs.utilities;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLConnUtils {

public static Connection getMySQLConnection()
        throws ClassNotFoundException, SQLException {
 
    // Note: Change the connection parameters accordingly.
    String hostName = "localhost";
    String dbName = "carpoolingdb";
    String userName = "root";
    String password = "";
    return getMySQLConnection(hostName, dbName, userName, password);
}

public static Connection getMySQLConnection(String hostName, String dbName,
        String userName, String password) throws SQLException,
        ClassNotFoundException {
   
    // Declare the class Driver for MySQL DB
    // This is necessary with Java 5 (or older)
    // Java6 (or newer) automatically find the appropriate driver.
    // If you use Java> 5, then this line is not needed.
    Class.forName("com.mysql.jdbc.Driver");


    // URL Connection for MySQL
    // Example: jdbc:mysql://localhost:3306/simplehr
    String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;

    Connection conn = DriverManager.getConnection(connectionURL, userName,
            password);
    return conn;
}

public static void findUser(Connection conn, String userName) throws SQLException {

    String sql = "Select userid, fullname from users";

    PreparedStatement pstm = conn.prepareStatement(sql);
//   pstm.setString(1, userName);

    ResultSet rs = pstm.executeQuery();

    if (rs.next()) {
    	System.out.println(rs.getString("userid"));
    	System.out.println(rs.getString("fullname"));
      /*  String password = rs.getString("Password");
        String gender = rs.getString("Gender");
        UserAccount user = new UserAccount();
        user.setUserName(userName);
        user.setPassword(password);
        user.setGender(gender);
        return user;*/
    	
    }
}}