package mum.cs.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import mum.cs.model.Comment;

public class CommentImp {
	

	public static Comment saveComment(Comment comment, Connection con) {
		

		try {
			// Connection con = getConnection();
			Statement st;
			PreparedStatement preparedStmt = con.prepareStatement(
					"insert into comments (commentid, username, postid, comment, datecreated, dateupdated) values(?,?,?,?,?,?)");
			preparedStmt.setInt(1, comment.getCommentId());
			preparedStmt.setString(2, comment.getUserName());
			preparedStmt.setInt(3, comment.getPostId());
			preparedStmt.setString(4, comment.getComment());
			preparedStmt.setDate(5, comment.getDateCreated());
			preparedStmt.setDate(6, comment.getDateUpdated());
			preparedStmt.execute();

			// execute the preparedstatement
			// preparedStmt.execute();
			//con.close();
			return comment;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static boolean deleteComment(Comment comment, Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			st = con.createStatement();
			PreparedStatement stmt = con.prepareStatement("delete from comments where commentid = ?");
			stmt.setInt(1, comment.getCommentId());
			stmt.execute();
			//con.close();
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	public static Comment updateComment(Comment comment, Connection con) {
		// prepareStatement("update commentloyee set firstName=?, lastName=?,
		// department=?, commentlDate=? , gender=? , email=?,userName=?,password=?
		// where id = ?");

		try {
			Statement st;
			st = con.createStatement();

			// String query = "update rent set status = ? where carCarId = ? AND
			// commentormerCusId = ? AND rentDate = ?";
			PreparedStatement preparedStmt = con.prepareStatement(
					"update comments set username = ?, postid = ?, comment = ?, datecreated = ?, dateupdated = ? where commentid  = ? ");

			// for the set(put) value
			preparedStmt.setString(1, comment.getUserName());
			preparedStmt.setInt(2, comment.getPostId());
			preparedStmt.setString(3, comment.getComment());
			preparedStmt.setDate(4, comment.getDateCreated());
			preparedStmt.setDate(5, comment.getDateUpdated());
			
			preparedStmt.setInt(6, comment.getCommentId());

			preparedStmt.execute();

		//	con.close();
			return comment;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static Comment getCommentById(int commentId, Connection con) {

		Comment comment = null;

		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select commentid, username, postid, comment, datecreated, dateupdated from comments where commentid = ?");
			preparedStmt.setInt(1, commentId);
			rs = preparedStmt.executeQuery();

			// commentloyee comment;
			ArrayList<String> record;
			while (rs.next()) {
				comment = new Comment();
				// record = new ArrayList<String>();

				comment.setCommentId(rs.getInt(1));
				comment.setUserName(rs.getString(2));
				comment.setPostId(rs.getInt(3));
				comment.setComment(rs.getString(4));
				comment.setDateCreated(rs.getDate(5));
				comment.setDateUpdated(rs.getDate(6));
		
				// comments.add(record);
			}
		//	con.close();
			return comment;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	
	public static ArrayList<Comment> getCommentByUserName(String userName, Connection con) {
		
		ArrayList<Comment> comments = new ArrayList<Comment>();
		
		boolean found=false;

	
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select commentid, username, postid, comment, datecreated, dateupdated from comments where username = ? order by commentid desc ");
			preparedStmt.setString(1, userName);
			rs = preparedStmt.executeQuery();

			// commentloyee comment;
			ArrayList<String> record;
			while (rs.next()) {
				found=true;
				Comment comment = new Comment();

				// record = new ArrayList<String>();

				comment.setCommentId(rs.getInt(1));
				comment.setUserName(rs.getString(2));
				comment.setPostId(rs.getInt(3));
				comment.setComment(rs.getString(4));
				comment.setDateCreated(rs.getDate(5));
				comment.setDateUpdated(rs.getDate(6));
		
				comments.add(comment);
				// comments.add(record);
			}
		//	con.close();
			if (found)
			return comments;
			else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	public static ArrayList<Comment> getCommentByPostId(int postId, Connection con) {
		
		ArrayList<Comment> comments = new ArrayList<Comment>();
		
		boolean found=false;

	
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select commentid, username, postid, comment, datecreated, dateupdated from comments where postid = ? order by commentid desc ");
			preparedStmt.setInt(1, postId);
			rs = preparedStmt.executeQuery();

			// commentloyee comment;
			ArrayList<String> record;
			while (rs.next()) {
				found=true;
				Comment comment = new Comment();

				// record = new ArrayList<String>();

				comment.setCommentId(rs.getInt(1));
				comment.setUserName(rs.getString(2));
				comment.setPostId(rs.getInt(3));
				comment.setComment(rs.getString(4));
				comment.setDateCreated(rs.getDate(5));
				comment.setDateUpdated(rs.getDate(6));
		
				comments.add(comment);
				// comments.add(record);
			}
		//	con.close();
			if (found)
			return comments;
			else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	
	
	public static ArrayList<Comment> getAllComment(Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> comments = new
		// ArrayList<ArrayList<String>>();
		boolean found =false;
		ArrayList<Comment> comments = new ArrayList<Comment>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select commentid, username, postid, comment, datecreated, dateupdated from comments");
			// preparedStmt.setString(1, commentId);
			rs = preparedStmt.executeQuery();

			// commentloyee comment;
			ArrayList<String> record;
			while (rs.next()) {
				found=true;
				Comment comment = new Comment();


				comment.setCommentId(rs.getInt(1));
				comment.setUserName(rs.getString(2));
				comment.setPostId(rs.getInt(3));
				comment.setComment(rs.getString(4));
				comment.setDateCreated(rs.getDate(5));
				comment.setDateUpdated(rs.getDate(6));

				comments.add(comment);
			}
		//	con.close();
			if(found)
			return comments;
			else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}
	
	public static void main(String[] args )
	{
		
	}

}
