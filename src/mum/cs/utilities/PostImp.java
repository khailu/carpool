package mum.cs.utilities;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tpostlate file, choose Tools | Tpostlates
 * and open the tpostlate in the editor.
 */

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import mum.cs.model.Post;

/**
 *
 * @author Henok B
 */
public class PostImp {
	
	

	public static Post savePost(Post post,Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			PreparedStatement preparedStmt = con.prepareStatement(
					"insert into posts (postid, username, posttype, source, destination, post, datecreated, dateupdated) values(?,?,?,?,?,?,?,?)");
			preparedStmt.setInt(1, post.getPostid());
			preparedStmt.setString(2, post.getUserName());
			preparedStmt.setString(3, post.getPosttype());
			preparedStmt.setString(4, post.getSource());
			preparedStmt.setString(5, post.getDestination());
			preparedStmt.setString(6, post.getPost());
			preparedStmt.setDate(7, post.getDateCreated());
			preparedStmt.setDate(8, post.getDateUpdated());
			preparedStmt.execute();

			// execute the preparedstatement
			// preparedStmt.execute();
			//con.close();
			return post;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static boolean deletePost(Post post,Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			st = con.createStatement();
			PreparedStatement stmt = con.prepareStatement("delete from posts where postid = ?");
			stmt.setInt(1, post.getPostid());
			stmt.execute();
			//con.close();
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	public  static Post updatePost(Post post,Connection con) {
		// prepareStatement("update postloyee set firstName=?, lastName=?,
		// department=?, postlDate=? , gender=? , email=?,userName=?,password=?
		// where id = ?");

		try {
			Statement st;
			st = con.createStatement();

			// String query = "update rent set status = ? where carCarId = ? AND
			// postormerCusId = ? AND rentDate = ?";
			PreparedStatement preparedStmt = con.prepareStatement(
					"update posts set username  = ?, posttype  = ?, source  = ?, destination  = ?, post = ?, datecreated = ?, dateupdated  = ? where postid  = ? ");

			// for the set(put) value
			preparedStmt.setString(1, post.getUserName());
			preparedStmt.setString(2, post.getPosttype());
			preparedStmt.setString(3, post.getSource());
			preparedStmt.setString(4, post.getDestination());
			preparedStmt.setString(5, post.getPost());
			preparedStmt.setDate(6, post.getDateCreated());
			preparedStmt.setDate(7, post.getDateUpdated());

			preparedStmt.setInt(8, post.getPostid());

			preparedStmt.execute();

		//	con.close();
			return post;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static Post getPostById(int postId,Connection con) {

		Post post = null;

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> posts = new
		// ArrayList<ArrayList<String>>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select postid, username, posttype, source, destination, post, datecreated, dateupdated from posts where postid = ?");
			preparedStmt.setInt(1, postId);
			rs = preparedStmt.executeQuery();

			// postloyee post;
			ArrayList<String> record;
			while (rs.next()) {
				post = new Post();
				// record = new ArrayList<String>();

				post.setPostid(rs.getInt(1));
				post.setUserName(rs.getString(2));
				post.setPosttype(rs.getString(3));
				post.setSource(rs.getString(4));
				post.setDestination(rs.getString(5));
				post.setPost(rs.getString(6));
				post.setDateCreated(rs.getDate(7));
				post.setDateUpdated(rs.getDate(8));

				// posts.add(record);
			}
		//	con.close();
			return post;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public static ArrayList<Post> getAllPost(Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> posts = new
		// ArrayList<ArrayList<String>>();
		boolean found =false;
		ArrayList<Post> posts = new ArrayList<Post>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select postid, username, posttype, source, destination, post, datecreated, dateupdated from posts order by postid desc");
			// preparedStmt.setString(1, postId);
			rs = preparedStmt.executeQuery();

			// postloyee post;
			ArrayList<String> record;
			while (rs.next()) {
				found=true;
				Post post = new Post();

				post.setPostid(rs.getInt(1));
				post.setUserName(rs.getString(2));
				post.setPosttype(rs.getString(3));
				post.setSource(rs.getString(4));
				post.setDestination(rs.getString(5));
				post.setPost(rs.getString(6));
				post.setDateCreated(rs.getDate(7));
				post.setDateUpdated(rs.getDate(8));

				posts.add(post);
			}
		//	con.close();
			if (found==true)
			return posts;
			else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public static ArrayList<Post> getPostByType(String postType ,Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> posts = new
		// ArrayList<ArrayList<String>>();
		ArrayList<Post> posts = new ArrayList<Post>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select postid, username, posttype, source, destination, post, datecreated, dateupdated from posts where posttype = ? order by postid desc");
			preparedStmt.setString(1, postType);
			rs = preparedStmt.executeQuery();

			// postloyee post;
			ArrayList<String> record;
			while (rs.next()) {
				Post post = new Post();

				post.setPostid(rs.getInt(1));
				post.setUserName(rs.getString(2));
				post.setPosttype(rs.getString(3));
				post.setSource(rs.getString(4));
				post.setDestination(rs.getString(5));
				post.setPost(rs.getString(6));
				post.setDateCreated(rs.getDate(7));
				post.setDateUpdated(rs.getDate(8));

				posts.add(post);
			}
		//	con.close();
			return posts;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public static ArrayList<Post> getPostByTypeAndUserName(String postType, String userName ,Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> posts = new
		// ArrayList<ArrayList<String>>();
		ArrayList<Post> posts = new ArrayList<Post>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select postid, username, posttype, source, destination, post, datecreated, dateupdated from posts where posttype = ? and username =? order by postid desc");
			preparedStmt.setString(1, postType);
			preparedStmt.setString(2, userName);
			rs = preparedStmt.executeQuery();

			// postloyee post;
			ArrayList<String> record;
			while (rs.next()) {
				Post post = new Post();

				post.setPostid(rs.getInt(1));
				post.setUserName(rs.getString(2));
				post.setPosttype(rs.getString(3));
				post.setSource(rs.getString(4));
				post.setDestination(rs.getString(5));
				post.setPost(rs.getString(6));
				post.setDateCreated(rs.getDate(7));
				post.setDateUpdated(rs.getDate(8));

				posts.add(post);
			}
		//	con.close();
			return posts;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	
	public static ArrayList<Post> getPostByUserName(String userName ,Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> posts = new
		// ArrayList<ArrayList<String>>();
		ArrayList<Post> posts = new ArrayList<Post>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select postid, username, posttype, source, destination, post, datecreated, dateupdated from posts where username = ? order by postid desc");
			preparedStmt.setString(1, userName);
			rs = preparedStmt.executeQuery();

			// postloyee post;
			ArrayList<String> record;
			while (rs.next()) {
				Post post = new Post();

				post.setPostid(rs.getInt(1));
				post.setUserName(rs.getString(2));
				post.setPosttype(rs.getString(3));
				post.setSource(rs.getString(4));
				post.setDestination(rs.getString(5));
				post.setPost(rs.getString(6));
				post.setDateCreated(rs.getDate(7));
				post.setDateUpdated(rs.getDate(8));

				posts.add(post);
			}
		//	con.close();
			return posts;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	/*public static void main(String[] args) throws ParseException {
		PostImp managepost = new PostImp();
			Post post1 = new Post();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		// Date d = (Date) sdf.parse("21/12/2012");
		// post1.setDateCreated(d);
		// post1.setDateUpdated(d);
		post1.setDestination("dddd");
		post1.setPost("pppp");
		post1.setPostid(7);
		post1.setPosttype("rrr");
		post1.setUserid(1);
		post1.setSource("cccc");

		managepost.savePost(post1);
		System.out.println("saved 3");
		post1.setDestination("ccc");
		managepost.updatePost(post1);	
		System.out.println("updated destination two cccc");
		
		Post post2=managepost.getPostById(6);	
		System.out.println("post2 destination two destination"+post2.getDestination());
		managepost.deletePost(post2);
		System.out.println("Deleteddddd post2");
		
		ArrayList<Post> data=managepost.getAllPost();
		
		System.out.println(data.size());



	}*/

}
