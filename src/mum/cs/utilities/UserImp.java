package mum.cs.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.sql.Date;
import mum.cs.model.User;

public class UserImp {
	
	
	

	public static User saveUser(User user,Connection con) {

		
		try {
			// Connection con = getConnection();
			Statement st;
			PreparedStatement preparedStmt = con.prepareStatement(
					"insert into users (userid, username, password, fullname, gender, state, city, street, zipcode, birthyear, email, datecreated, dateupdated) values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
			preparedStmt.setInt(1, user.getUserId()); 
			preparedStmt.setString(2, user.getUserName());
			preparedStmt.setString(3, user.getPassword());
			preparedStmt.setString(4, user.getFullName());
			preparedStmt.setString(5, user.getGender());
			preparedStmt.setString(6, user.getState());
			preparedStmt.setString(7, user.getCity());
			preparedStmt.setString(8, user.getStreet());
			preparedStmt.setInt(9, user.getZipcode());
			preparedStmt.setDate(10, (Date) user.getBirthYear()); 
			preparedStmt.setString(11, user.getEmail());
			preparedStmt.setDate(12, (Date) user.getDateCreated());
			preparedStmt.setDate(13, (Date) user.getDateUpdated());
			preparedStmt.execute();

			// execute the preparedstatement
			// preparedStmt.execute();
			//con.close();
			return user;

		} catch (Exception ex) {
			System.out.println("errrrrrr"+ex.getMessage());
			ex.printStackTrace();
			return null;
		}

	}

	public static boolean deleteUser(User user,Connection con) {

		try {
			// Connection con = getConnection();
			Statement st;
			st = con.createStatement();
			PreparedStatement stmt = con.prepareStatement("delete from users where username = ?");
			stmt.setString(1, user.getUserName());
			stmt.execute();
			//con.close();
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}

	public static User updateUser(User user,Connection con) {
		// prepareStatement("update userloyee set firstName=?, lastName=?,
		// department=?, userlDate=? , gender=? , email=?,userName=?,password=?
		// where id = ?");

		try {
			Statement st;
			st = con.createStatement();

			// String query = "update rent set status = ? where carCarId = ? AND
			// userormerCusId = ? AND rentDate = ?";
			PreparedStatement preparedStmt = con.prepareStatement(
					"update users set  password = ?, fullname = ?, gender = ?, state = ?,city = ?, street = ?, zipcode = ?, birthyear = ?, email = ?, datecreated = ?, dateupdated = ?    where  username = ? ");

			// for the set(put) value
			preparedStmt.setString(1, user.getPassword());
			preparedStmt.setString(2, user.getFullName());
			preparedStmt.setString(3, user.getGender());
			preparedStmt.setString(4, user.getState());
			preparedStmt.setString(5, user.getCity());
			preparedStmt.setString(6, user.getStreet());
			preparedStmt.setInt(7, user.getZipcode());
			preparedStmt.setDate(8, (Date) user.getBirthYear()); 
			preparedStmt.setString(9, user.getEmail());
			preparedStmt.setDate(10, (Date) user.getDateCreated());
			preparedStmt.setDate(11, (Date) user.getDateUpdated());
			
			preparedStmt.setString(12, user.getUserName());


			preparedStmt.execute();

		//	con.close();
			return user;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public static User getUser(String userName,Connection con) {

		User user = null;

		try {

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select userid, username, password, fullname, gender, state, city, street, zipcode, birthyear, email, datecreated, dateupdated from users where username = ?");
			preparedStmt.setString(1, userName);
			rs = preparedStmt.executeQuery();


			// userloyee user;
			ArrayList<String> record;
			while (rs.next()) {
				// record = new ArrayList<String>();
				user = new User();
				user.setUserId(rs.getInt(1)); 
				user.setUserName(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFullName(rs.getString(4));
				 user.setGender(rs.getString(5));
					user.setState(rs.getString(6));
					user.setCity(rs.getString(7));
				user.setStreet(rs.getString(8));
				 user.setZipcode(rs.getInt(9));
				user.setBirthYear(rs.getDate(10));
				 user.setEmail(rs.getString(11));
				user.setDateCreated(rs.getDate(12));
				 user.setDateUpdated(rs.getDate(13));

					// users.add(record);
			}
		//	con.close();
			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	public static User getUser(String userName, String passowrd,Connection con) {

		User user = null;

		try {

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select userid, username, password, fullname, gender, state, city, street, zipcode, birthyear, email, datecreated, dateupdated from users where username = ? and password = ? ");
			preparedStmt.setString(1, userName);
			preparedStmt.setString(2, passowrd);
			rs = preparedStmt.executeQuery();
			/*if(rs.getRow()==0){
				return null;
			}*/
			// userloyee user;
			ArrayList<String> record;
			while (rs.next()) {
				
				user=new User();
				// record = new ArrayList<String>();

				user.setUserId(rs.getInt(1)); 
				user.setUserName(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFullName(rs.getString(4));
				 user.setGender(rs.getString(5));
					user.setState(rs.getString(6));
					user.setCity(rs.getString(7));
				user.setStreet(rs.getString(8));
				 user.setZipcode(rs.getInt(9));
				user.setBirthYear(rs.getDate(10));
				 user.setEmail(rs.getString(11));
				user.setDateCreated(rs.getDate(12));
				 user.setDateUpdated(rs.getDate(13));

					// users.add(record);
			}
		//	con.close();
			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	public static ArrayList<User> getAllUser(Connection con) {

		// public ArrayList<ArrayList<String>> SearchFromTable(String query,
		// String fieldvalue, ArrayList<String> headTable) {
		// ArrayList<ArrayList<String>> users = new
		// ArrayList<ArrayList<String>>();
		boolean found=false;
		ArrayList<User> users = new ArrayList<User>();
		try {
			// Connection connection = getConnection();

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select userid, username, password, fullname, gender, state, street, city, zipcode, birthyear, email, datecreated, dateupdated from users");
			// preparedStmt.setString(1, userId);
			rs = preparedStmt.executeQuery();
			/*if(rs. .getRow()==0){
				return null;
			}*/
			// userloyee user;
			ArrayList<String> record;
			while (rs.next()) {
				found=true;
				User user = new User();

				user.setUserId(rs.getInt(1)); 
				user.setUserName(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFullName(rs.getString(4));
				 user.setGender(rs.getString(5));
					user.setState(rs.getString(6));
					user.setCity(rs.getString(7));
				user.setStreet(rs.getString(8));
				 user.setZipcode(rs.getInt(9));
				user.setBirthYear(rs.getDate(10));
				 user.setEmail(rs.getString(11));
				user.setDateCreated(rs.getDate(12));
				 user.setDateUpdated(rs.getDate(13));

				users.add(user);
			}
		//	con.close();
			if(found==true)
			return users;
			else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public static User getUserEmail(String email,Connection con) {

		User user = null;

		try {

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			PreparedStatement preparedStmt = con.prepareStatement(
					"select userid, username, password, fullname, gender, state, city, street, zipcode, birthyear, email, datecreated, dateupdated from users where email = ?");
			preparedStmt.setString(1, email);
			rs = preparedStmt.executeQuery();

			// userloyee user;
			ArrayList<String> record;
			while (rs.next()) {
				// record = new ArrayList<String>();
				user = new User();

				user.setUserId(rs.getInt(1)); 
				user.setUserName(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFullName(rs.getString(4));
				 user.setGender(rs.getString(5));
					user.setState(rs.getString(6));
					user.setCity(rs.getString(7));
				user.setStreet(rs.getString(8));
				 user.setZipcode(rs.getInt(9));
				user.setBirthYear(rs.getDate(10));
				 user.setEmail(rs.getString(11));
				user.setDateCreated(rs.getDate(12));
				 user.setDateUpdated(rs.getDate(13));

					// users.add(record);
			}
		//	con.close();
			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


}
