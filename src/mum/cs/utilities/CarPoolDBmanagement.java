package mum.cs.utilities;

import java.sql.Connection;
import java.sql.DriverManager;

public class CarPoolDBmanagement {

    public Connection getConnection() {
        Connection con;

        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/carpoolingdb?zeroDateTimeBehavior=convertToNull", "root", "root");

            // con = DriverManager.getConnection("jdbc:mysql://localhost/test_db", "root","");
            return con;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}