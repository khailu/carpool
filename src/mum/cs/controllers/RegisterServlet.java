package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mum.cs.model.User;
import mum.cs.utilities.MyUtils;
import mum.cs.utilities.UserImp;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*
		 * if(request.getSession(). getAttribute("user")!=null){
		 * RequestDispatcher dispatcher =
		 * this.getServletContext().getRequestDispatcher("/Home");
		 * dispatcher.forward(request, response); response.sendRedirect("Home");
		 * return;
		 * 
		 * }
		 */
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection conn = MyUtils.getStoredConnection(request);

		Date date = null;
		request.getSession().invalidate();
		String birthYearCal = request.getParameter("regBirthYear");
		System.out.println("birthday " + birthYearCal);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		try {
			date = formatter.parse(birthYearCal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDateBirthDay = new java.sql.Date(date.getTime());

		boolean error = false;
		String userName = request.getParameter("regUsername");
		String email = request.getParameter("regEmail");
		System.out.println("username error" + UserImp.getUser(userName, conn));

		if (UserImp.getUser(userName, conn) != null) {
			request.setAttribute("errorUserName", "UserName already exist");
			error = true;
			System.out.println("username error");
		}

		if (UserImp.getUserEmail(email, conn) != null) {
			request.setAttribute("errorEmail", "Email already exist");
			error = true;
			System.out.println("email error");
		}
		String password = request.getParameter("regPassword");
		String confPassword = request.getParameter("regConfPassword");
		if(!confPassword.equals(password)){
			request.setAttribute("errorConfirm", "Password Didn't match");
			error = true;
		}
		if (error == true) {
			System.out.println("error");
			validateRegistration(request, response);
			return;
		}
		System.out.println("success");
		
		String fullName = request.getParameter("regFullName");
		String gender = request.getParameter("regGender");
		String state = request.getParameter("regState");

		// String city=request.getParameter("regCity");
		String city = "";
		String street = request.getParameter("regStreet");
		int zipcode = Integer.valueOf(request.getParameter("regZipcode"));

		User user = new User(userName, password, fullName, gender, state, city, street, zipcode, sqlDateBirthDay, email,
				null, null);

		UserImp.saveUser(user, conn);

		request.getSession().setAttribute("user", user);
		/*RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Home");
		dispatcher.forward(request, response);*/
		response.sendRedirect("Home");

	}

	public void validateRegistration(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("email", request.getParameter("regEmail"));
		request.setAttribute("userName", request.getParameter("regUsername"));
		
request.setAttribute("fullName", request.getParameter("regFullName"));
request.setAttribute("gender", request.getParameter("regGender"));
request.setAttribute("state", request.getParameter("regState"));
request.setAttribute("street", request.getParameter("regStreet"));
request.setAttribute("zipcode", request.getParameter("regZipcode"));
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
	}

}
