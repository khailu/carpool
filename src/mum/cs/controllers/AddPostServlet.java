package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mum.cs.model.Post;
import mum.cs.model.User;
import mum.cs.utilities.MyUtils;
import mum.cs.utilities.PostImp;

/**
 * Servlet implementation class AddPostServlet
 */
@WebServlet("/AddPost")
public class AddPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//select postid, username, posttype, source, destination, post, datecreated, dateupdated)
		
		 Connection conn = MyUtils.getStoredConnection(request);
		String userName=((User)request.getSession().getAttribute("user")).getUserName();
		String postType=(String)request.getParameter("postType");
		String source=(String)request.getParameter("from");
		String destination=(String)request.getParameter("where");
		String postBody=(String)request.getParameter("postBody");
		
		


		
		
		
	//	Date now=new Date();
	//	java.sql.Date sqlNow=now.set
		
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDatenow = new java.sql.Date(utilDate .getTime());
		
		Post post=new Post(userName,postBody,postType,sqlDatenow,null,source,destination);
		
		PostImp.savePost(post, conn);
		
		response.getWriter().write("Done");
	}

}
