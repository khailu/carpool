package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mum.cs.model.User;
import mum.cs.utilities.MyUtils;
import mum.cs.utilities.UserImp;

/**
 * Servlet implementation class UpdateUserServlet
 */
@WebServlet("/UpdateUser")
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 Connection conn = MyUtils.getStoredConnection(request);
		boolean error=false;
	//	 String userName = request.getParameter("regUsername");
		String userName=((User)request.getSession().getAttribute("user")).getUserName();
		  String email=request.getParameter("regEmail");
		  System.out.println("success");
	        String password = request.getParameter("regPassword");
	        String fullName=request.getParameter("regFullName");
	        String gender=request.getParameter("regGender");
	        String state=request.getParameter("regState");
	        String city=request.getParameter("regCity");
	        String street=request.getParameter("regStreet");
	        int zipcode=Integer.valueOf(request.getParameter("regZipcode"));
	        
	        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	        Date birthYear=null;
			try {
				birthYear = formatter.parse(  request.getParameter("regBirthYear"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      	System.out.println("username error" + UserImp.getUser(userName, conn));

		/* if(UserImp.getUser(userName, conn)!=null){
	        	request.setAttribute("errorUserName", "UserName already exist");
	        	error=true;
	        	System.out.println("username error");
	        }*/
	      	
	        if(UserImp.getUserEmail(email, conn)!=null){
	        	request.setAttribute("errorEmail", "Email already exist");
	        	error=true;
	        	System.out.println("email error");
	        }
	        if(error==true){
	        	System.out.println("error");
	        	
	        	HttpSession s=request.getSession();
				s.setAttribute("fullName", fullName);
				s.setAttribute("gender", gender);
				s.setAttribute("state", state);
				s.setAttribute("street", fullName);
				s.setAttribute("zipcode", zipcode);
				s.setAttribute("birthYear", birthYear);
	        	
	        	validateRegistration(request, response);
	        	return;
	        }
	       
	       
			
			
			
			
			
	        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	        Date dateobj = new Date();
	        
	        
	        

	        User user=new User(userName, password, fullName, gender, state,city, street, zipcode, null, email, null, null);
	      	        UserImp.updateUser(user, conn);
	       
	        
	       
	        
request.getSession().setAttribute("user", user);
       	RequestDispatcher dispatcher 
           = this.getServletContext().getRequestDispatcher("/ShowPost");
       	dispatcher.forward(request, response);
		
		
	}
	
public void validateRegistration(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		
		RequestDispatcher dispatcher  = this.getServletContext().getRequestDispatcher("/index.jsp");
    	dispatcher.forward(request, response);
	}

}
