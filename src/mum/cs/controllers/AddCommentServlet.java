package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mum.cs.model.Comment;
import mum.cs.model.User;
import mum.cs.utilities.CommentImp;
import mum.cs.utilities.MyUtils;

/**
 * Servlet implementation class AddCommentServlet
 */
@WebServlet("/AddComment")
public class AddCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 Connection conn = MyUtils.getStoredConnection(request);
		 int postId=Integer.valueOf(request.getParameter("postId"));
		String userName=((User)request.getSession().getAttribute("user")).getUserName();
			String commentBody=(String)request.getParameter("commentBody");
			Date date=new Date();
			
			java.sql.Date dateCreated=new java.sql.Date(date.getTime());
			Comment comment=new Comment(userName, postId, commentBody, null, null);
			CommentImp.saveComment(comment, conn);
			
			response.getWriter().write("hello");
			//Date
	}

}
