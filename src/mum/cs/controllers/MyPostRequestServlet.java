package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import mum.cs.model.Post;
import mum.cs.model.User;
import mum.cs.utilities.CarPoolingMarshaller;
import mum.cs.utilities.MyUtils;
import mum.cs.utilities.PostImp;

/**
 * Servlet implementation class MyPostRequestServlet
 */
@WebServlet("/MyPostRequest")
public class MyPostRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyPostRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 Connection conn = MyUtils.getStoredConnection(request);
			
			//	 System.out.println("commentttttttttttt"+CommentImp.getAllComment(conn).size());
				 
				 
				 
				 User user=(User)request.getSession().getAttribute("user");
				 
					List<Post> data=PostImp.getPostByTypeAndUserName("request", user.getUserName(), conn);
					
					System.out.println(data.size());

					String responseJson=null;
					try {
						responseJson = CarPoolingMarshaller.getJsonFromObject(data);
					} catch (JsonGenerationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//PrintWriter out = response.getWriter();
					System.out.println(responseJson);
					List<Object> data2=null;

					response.setContentType("application/json");
				
					response.setCharacterEncoding("UTF-8");
			        response.getWriter().write(responseJson);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
