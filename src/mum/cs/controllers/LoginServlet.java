package mum.cs.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mum.cs.model.User;
import mum.cs.utilities.*;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//check it the users has already logged in
		if(request.getSession().    getAttribute("user")!=null){
			/*RequestDispatcher dispatcher 
            = this.getServletContext().getRequestDispatcher("/Home");
        	dispatcher.forward(request, response);*/
			response.sendRedirect("Home");
        	return;
			
		}
		doPost( request,  response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	System.out.println("hellllll");
		String hasError="false";
		String errorString = "";
		
		 String userName = request.getParameter("userName");
	        String password = request.getParameter("password");
	       
	   
	        if (userName == null || password == null
	                 || userName.length() == 0 || password.length() == 0) {
	        	hasError="true";
	        	errorString = "Required username and/or password!";
	       // 	response.setContentType("text/html");
	        	
	         //   response.setCharacterEncoding("UTF-8");
	           // response.getWriter().write(hasError);
	        	errorValidate(request, response, errorString);
	        }
	        else{
	         	 Connection conn = MyUtils.getStoredConnection(request);

	     
	  	        	if(UserImp.getUser(userName,password,conn)==null){
	        		System.out.println("err333");
				hasError="true";
	        	errorString = "Invalid Username and/or Password";
response.setContentType("text/html");
errorValidate(request, response, errorString);
	            /*response.setCharacterEncoding("UTF-8");
	            response.getWriter().write(hasError);*/
			}
	        else{
	        	User user =(User)UserImp.getUser(userName,password,conn);
	        	request.getSession().setAttribute("user", user);
	        	/*RequestDispatcher dispatcher 
	            = this.getServletContext().getRequestDispatcher("/Home");
	        	dispatcher.forward(request, response);*/
	        	response.sendRedirect("Home");
	        }
	        }  
		
		try{
			//findUser(conn,"kaleb");
		}
		catch (Exception e) {
			// TODO: handle exception
			
		}
//HttpSession s=request.setAttribute("myConection", conn);
		
		
	}
	
	public void errorValidate(HttpServletRequest request, HttpServletResponse response,String errorString) throws ServletException, IOException {
		
		
		request.setAttribute("errorString", errorString);
		
		RequestDispatcher dispatcher 
        = this.getServletContext().getRequestDispatcher("/index.jsp");
    	dispatcher.forward(request, response);
	}
	
	

}
