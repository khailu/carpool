package mum.cs.model;

import java.sql.Date;

public class Comment {
	
	private String userName;
	private int commentId;
	private int postId;
	private String comment;
	private Date dateCreated;
	private Date dateUpdated;
	
	public Comment(){}

	
	



	public Comment(String userName, int commentId, int postId, String comment, Date dateCreated, Date dateUpdated) {
		super();
		this.userName = userName;
		this.commentId = commentId;
		this.postId = postId;
		this.comment = comment;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
	}


	public Comment(String userName, int postId, String comment, Date dateCreated, Date dateUpdated) {
		super();
		this.userName = userName;
		
		this.postId = postId;
		this.comment = comment;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
	}




	public String getUserName() {
		return userName;
	}






	public void setUserName(String userName) {
		this.userName = userName;
	}






	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	

	
	
	
	
}
