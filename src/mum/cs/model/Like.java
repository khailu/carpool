package mum.cs.model;

import java.sql.Date;

public class Like {
	private int likeId;
	private String userName;
	private int postId;
	private Date datecreated;
	private Date dateupdated;
	
	public Like(){}

	
	
	
	public Like(int likeId, String userName, int postId, Date datecreated, Date dateupdated) {
		super();
		this.likeId = likeId;
		this.userName = userName;
		this.postId = postId;
		this.datecreated = datecreated;
		this.dateupdated = dateupdated;
	}




	public int getLikeId() {
		return likeId;
	}

	public void setLikeId(int likeId) {
		this.likeId = likeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public Date getDatecreated() {
		return datecreated;
	}

	public void setDatecreated(Date datecreated) {
		this.datecreated = datecreated;
	}

	public Date getDateupdated() {
		return dateupdated;
	}

	public void setDateupdated(Date dateupdated) {
		this.dateupdated = dateupdated;
	}

	
	


	



}
