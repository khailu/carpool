package mum.cs.model;

import java.sql.Date;

public class Post {
	private int postid;
	private String userName;
	private String post;
	private String posttype;
	private Date dateCreated;
	private Date dateUpdated;
	private String source;
	private String destination;
	
	
	public Post(){}


	
	public Post(int postid, String userName, String post, String posttype, Date dateCreated, Date dateUpdated,
			String source, String destination) {
		super();
		this.postid = postid;
		this.userName = userName;
		this.post = post;
		this.posttype = posttype;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.source = source;
		this.destination = destination;
	}

	public Post( String userName, String post, String posttype, Date dateCreated, Date dateUpdated,
			String source, String destination) {
		super();
		
		this.userName = userName;
		this.post = post;
		this.posttype = posttype;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
		this.source = source;
		this.destination = destination;
	}

	public int getPostid() {
		return postid;
	}


	public void setPostid(int postid) {
		this.postid = postid;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPost() {
		return post;
	}


	public void setPost(String post) {
		this.post = post;
	}


	public String getPosttype() {
		return posttype;
	}


	public void setPosttype(String posttype) {
		this.posttype = posttype;
	}


	public Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}


	public Date getDateUpdated() {
		return dateUpdated;
	}


	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	

	
	
}
