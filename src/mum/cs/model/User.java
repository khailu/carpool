package mum.cs.model;

import java.util.Date;

public class User {
	private int userId;
	private String userName;
	private String password;
	private String fullName;
	private String gender;
	private String state;
	private String city;
	private String street;
	private int zipcode;
	private Date birthYear;
	private String email;
	private Date dateCreated;
	private Date dateUpdated;
	
	public User()
	{
		
	}
	
	

	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public User(String userName, String password, String fullName, String gender, String state,String city,
			String street, int zipcode, Date birthYear, String email, Date dateCreated,
			Date dateUpdated) {
		super();
		
		this.userName = userName;
		this.password = password;
		this.fullName = fullName;
		this.gender = gender;
		this.state = state;
		this.city = city;
		this.street = street;
		this.zipcode = zipcode;
		this.birthYear = birthYear;
		this.email = email;
		this.dateCreated = dateCreated;
		this.dateUpdated = dateUpdated;
	}



	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getZipcode() {
		return zipcode;
	}

	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public Date getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Date birthYear) {
		this.birthYear = birthYear;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	


	
	
	
	
}
